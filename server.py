from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
CORS(app)

@app.route("/")
def helloWorld():
  return jsonify({"Hello":"Hello World"})

@app.route('/bot', methods = ['POST'])
def reply():
    with open("response.json") as json_file:
        json_data = json.load(json_file)    
        try:
            response = json_data[request.get_json()["message"].lower()]
        except KeyError:
            response = json_data["@no_input"]
    return jsonify(response)
app.run()